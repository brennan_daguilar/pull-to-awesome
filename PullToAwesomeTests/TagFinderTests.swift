//
//  TagFinderTests.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 1/30/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit
import XCTest

extension NSRange: Equatable {
}
public func ==(lhs: NSRange, rhs: NSRange) -> Bool {
    return lhs.location == rhs.location && lhs.length == rhs.length
}

let testWords = ["abrupt", "belief", "boycott", "brochure", "budgeting", "buffalo", "accede", "accost", "acquittal", "adjourn", "afterward", "anachronism", "analysis", "attire", "aversion", "belie", "battery", "banquet", "banana", "accelerate", "accidentally", "accompanied", "accumulate", "accuse", "amphibian", "ancestry", "ancestry", "ankle", "anywhere", "arrival", "artificial", "attractive", "aspect", "ballot", "bondage", "calendar", "conceive", "commitment", "clientele", "classification", "cheddar", "cartage", "capitalization", "difficulty", "criticize", "dehydration", "droll", "disposition", "disposal", "didactic", "despoil", "demean", "delinquent", "delegate", "diagnosis", "development", "civilization", "civil", "chauffeur", "chair", "celebration", "ceiling", "cashier", "campaign", "civilize", "community", "develop", "deceive", "duplicate", "cylinder", "culprit", "cuddle", "critic", "counterfeit", "durable", "fulfill", "expediency", "expedite", "expenditure", "extension", "extract", "extraordinary", "exude", "familiar", "fiend", "flamboyant", "flexible", "foreign", "forfeit", "foundation", "futurity", "eon", "exclude", "evidently", "exhale", "elicit", "eliminate", "elliptical", "embarrass", "eminent", "employee", "emulate", "enable", "encode", "encyclopedia", "engineer", "enslave", "equipping", "evanescent", "fastidious", "guild", "indulge", "infection", "inferior", "inflate", "informal", "injury", "inscription", "insecure", "insinuate", "institution", "instrument", "intelligible", "interior", "invalid", "involved", "irrefutable", "induce", "index", "inaugural", "height", "heavily", "gaseous", "gauge", "genuine", "graduate", "grocery", "grovel", "handkerchief", "hearty", "hosiery", "hygiene", "illegal", "illiterate", "imbecile", "imitation", "incautious", "jeopardize", "lying", "lovely", "logical", "literature", "library", "laborious", "laboratory", "labeling", "knowledge", "kindergarten", "khaki", "kerosene", "kangaroo", "jurist", "journey", "jewelry", "lyric", "obey", "persist", "persuade", "perspire", "pleat", "plethora", "policeman", "potato", "prairie", "predecessor", "presage", "pretzel", "prostrate", "protocol", "puddle", "putter", "quandary", "particular", "parse", "pamphlet", "obligate", "obnoxious", "obscure", "obscure", "occasion", "occasionally", "occur", "odor", "offense", "oneself", "opossum", "opponent", "optimism", "organization", "organize", "pageant", "quantity", "rabid", "security", "serviceable", "shriek", "shuffle", "shutter", "siege", "skeptical", "stereo", "sticky", "strenuous", "strident", "submission", "surfeit", "surmount", "syllable", "syrup", "secretarial", "scrabble", "scissors", "ransack", "rapport", "raucous", "receipt", "recline", "recluse", "recognize", "recreation", "recurring", "rodeo", "rotund", "safety", "salvation", "saturate", "savor", "scholastic", "system", "visitor", "vagrant", "varnish", "vinegar", "visible", "void", "waiver", "warranty", "wholly", "willful", "yacht", "yield", "zenith", "vacuum", "usurp", "weather", "tailor", "tangible", "tariff", "tendency", "therefore", "thoroughbred", "throughout", "tornado", "tornado", "turmoil", "unique", "zoology", "accept", "asterisk", "audible", "acknowledgment", "abbreviate", "absorbent", "affects", "allotment", "allotted", "altercation", "amass", "amorous", "angular", "arrangement", "assignment", "attorneys", "asphalt", "architect", "antiseptic", "access", "accessible", "accessory", "accumulate", "acoustics", "adjust", "aerial", "aerial", "alien", "already", "amendment", "ancestor", "anecdote", "anonymous", "antidote", "antique", "aviator", "cancellation", "council", "counsel", "country", "creator", "customer", "cachet", "callus", "capricious", "centripetal", "chaste", "cinnamon", "collateral", "compensation", "contiguous", "corporal", "critique", "consider", "competent", "canvas", "canvass", "carburetor", "cashier", "catastrophe", "characteristic", "classic", "colleague", "college", "comedy", "commerce", "commercial", "committee", "communicate", "commuter", "compel", "customary", "daybreak", "disbursement", "discernible", "discrepancy", "disinterested", "disoblige", "dissociate", "distress", "divine", "domestic", "domesticate", "dominance", "disapproval", "diffidence", "detain", "deceive", "defendant", "deficient", "devour", "diagnosis", "diminish", "disappeared", "deficit", "depreciation", "desirable", "desolate", "diurnal", "eliminate", "extricate", "facsimile", "fantastic", "faulty", "federal", "feud", "flout", "fluorescent", "folklore", "forcible", "fortunately", "franchise", "frivolous", "frostbitten", "further", "extremity", "expression", "exotic", "encyclopedia", "familiar", "flexible", "ecstasy", "effect", "eject", "eligible", "encouragement", "epic", "equilibrium", "erroneous", "especially", "exceptional", "excessive", "existence", "easier", "genuine", "handsome", "haphazard", "harness", "hazardous", "headquarters", "homogenize", "horrific", "humidor", "handling", "handicapped", "galaxy", "gesture", "geology", "gigantic", "gnawing", "gorgeous", "grotesque", "gymnasium", "galling", "irresistible", "irrevocable", "issuing", "itemized", "judicious", "juror", "justifiable", "landslide", "legitimate", "leisure", "liaison", "license", "lieutenant", "loophole", "lunar", "luncheon", "irksome", "invariable", "interrogative", "jewelry", "library", "idiosyncrasy", "impatience", "incandescent", "inconsolable", "indelible", "inept", "influence", "innocence", "innumerable", "insistent", "insoluble", "integrity", "intensify", "impinge", "nineteenth", "noticeable", "notoriety", "nuisance", "numerator", "nylon", "obesity", "oblique", "obstinate", "obsolete", "opposite", "oregano", "overrate", "officious", "malefactor", "nonentity", "newsstand", "neutral", "optimism", "magnify", "malicious", "markup", "mattress", "mesmerize", "meteor", "metric", "mischievous", "misgiving", "modern", "mystery", "negligence", "neon", "modicum", "pageant", "quixotic", "radioactive", "rayon", "raze", "recently", "reconcile", "relevant", "relief", "repulse", "revive", "rhyme", "rhythm", "roommate", "roster", "questionnaire", "quest", "prospectus", "Parliament", "passable", "paucity", "penalty", "perseverance", "personality", "picnicking", "plaintiff", "poignancy", "poignant", "potential", "preceding", "precipice", "preoccupy", "rapacious", "vocal", "terrific", "thieves", "tragedy", "transient", "transmutation", "tyranny", "unacceptable", "unmoved", "usher", "utopia", "vengeance", "voucher", "withhold", "wrestle", "written", "schism", "statutory", "subvert", "solitary", "unique", "sanctuary", "sandwich", "scarcely", "schedule", "scholar", "schooner", "sedition", "semester", "seminary", "session", "shrine", "sieve", "signal", "sincerely", "soccer", "turpitud", "accident", "associate", "athlete", "audible", "abolish", "absence", "admissible", "advisor", "arise", "attorney", "autumn", "ascend", "approval", "appreciate", "adequate", "adopt", "advantage", "adventure", "adverb", "ailment", "alchemy", "algebra", "aluminum", "ancient", "awful", "bachelor", "crystal", "banjo", "baritone", "bauble", "belittle", "blonde", "buyer", "calligraphy", "centigram", "civilian", "clinch", "colander", "cologne", "contrary", "copier", "covet", "crept", "crutch", "creditor", "barbecue", "basin", "blanket", "blouse", "bonnet", "button", "cabinet", "cafeteria", "camouflage", "carburetor", "cement", "character", "coarse", "coffee", "common", "conscience", "contemporary", "crystallize", "dismiss", "dessert", "destination", "detail", "discuss", "daylight", "defender", "deny", "desecration", "detach", "directory", "desire", "desert", "descent", "display", "division", "dormitory", "duet", "debris", "decade", "defeat", "delicate", "delivery", "deposit", "disapprove", "illustration", "instead", "introduce", "irresistible", "janitor", "jeopardy", "judgment", "knack", "knead", "knives", "lacquer", "larynx", "inquire", "innocent", "imaginary", "imbalance", "imitate", "immigrant", "immovable", "immune", "impatient", "import", "impossible", "impress", "infinite", "limestone,manila", "periodical", "perish", "petition", "poem", "political", "positive", "possible", "poultry", "predict", "prefer", "preference", "private", "profit", "prominent", "promise", "punish", "perforated", "pavilion", "pastime", "manufacturer", "medicine", "Mercury", "monotonous", "naive", "narrative", "necessary", "notify", "obedience", "oblige", "operation", "orchid", "outfit", "owner", "packet", "paradoxical", "pursue", "quartet", "silhouette", "skillet", "sleuth", "solo", "solve", "somersault", "soprano", "source", "steady", "subdue", "submarine", "surgery", "survey", "swift", "service", "serious", "quiver", "raccoon", "release", "rely", "requirement", "research", "resist", "resource", "salmon", "saucer", "saucy", "sculptor", "senator", "sentence", "switch", "their", "vehicle", "venom", "verbal", "verify", "violence", "voluntary", "weevil", "wholesale", "wit", "vary", "unnecessary", "zero", "taunt", "technical", "testimonial", "there", "touch", "trousers", "tuxedo", "unaware", "women"]

var tags: [RequestTag] = []
let caseInsensitiveTagFinder = TagFinder(caseSensitive: false)
let fastFinder = ComplexTagFinder(caseSensitive: false)

class TagFinderTests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
        
        for var i = 0; i < countElements(testWords); i++ {
            let word = testWords[i]
            let tag = RequestTag(id:i, word:word, key:word+"_key", category:word+"_category", used:0, active:1)
            tags.append(tag)
            
        }
        fastFinder.addTags(tags)
        caseInsensitiveTagFinder.addTags(tags)
    }
    
    override func tearDown() {
        super.tearDown()
        tags = []
    }
    
    func testBlankString() {
        let subject = ""
        XCTAssertNil(caseInsensitiveTagFinder.findTagsInString(subject), "No tags should be found in a blank string")
    }
    
    func testIncompleteWord() {
        let subject = "verba"
        XCTAssertNil(caseInsensitiveTagFinder.findTagsInString(subject), "No tags should be found in the incomplete string")
    }
    
    func testCompleteWord() {
        let subject = "verbal"
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 0, length: 6), "The tag should be the first 6 characters")
    }
    
    func testCompleteWordAndSpace() {
        let subject = "verbal "
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 0, length: 6), "The tag should be the first 6 characters")
    }
    
    func testCompleteWordFollowedByNonTag() {
        let subject = "verbal pants"
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 0, length: 6), "The tag should be the first 6 characters")
    }
    
    func testNonTagFollowedByCompleteTag() {
        let subject = "pants verbal"
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 6, length: 6), "Tag range was incorrect")
    }
    
    func testTagFollowedByPunctuation() {
        let subject = "verbal,"
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 0, length: 6), "Tag range was incorrect")
    }
    
    func testTagPreceededByPunctuation() {
        let subject = "!verbal"
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 1, length: 6), "Tag range was incorrect")
    }
    
    func testTagMultipleConnectedPunctuation() {
        let subject = "This   ..... is not... kindergarten...!"
        let ranges = caseInsensitiveTagFinder.findTagsInString(subject)
        let range = ranges?[0]
        XCTAssertNotNil(range, "A tag should have been found")
        XCTAssertEqual(ranges!.count, 1, "Only one tag should have been found")
        XCTAssertEqual(range!, NSRange(location: 23, length: 12), "Tag range was incorrect")
    }
    
    func testBasicTagFinderPerformance() {
        self.measureBlock() {
            let subject = "Sriracha heirloom meggings small batch twee, selfies banana meh before they sold out. Four loko Neutra craft beer bitters heirloom aesthetic paleo, authentic street art cardigan flannel tilde selvage drinking vinegar. Food truck try-hard messenger bag, disrupt bespoke Marfa blog swag butcher ethical. Freegan four dollar toast before they sold out, Godard sartorial Banksy scenester cuddle Odd Future meh pour-over pickled taxidermy photo booth fulfill Marfa heirloom. Salvia you probably haven't heard of them Tumblr cliche, tofu Shoreditch distillery authentic swag. Sriracha fanny pack bitters try-hard, seitan taxidermy gluten-free tousled heirloom. Direct trade VHS cronut meggings, banh mi occupy fap Shoreditch messenger bag sustainable church-key expenditure blog readymade."
            
            caseInsensitiveTagFinder.findTagsInString(subject)
            
        }
    }
    
    func testFasterTagFinderPerformance() {
        self.measureBlock() {
            let subject = "Sriracha heirloom meggings small batch twee, selfies banana meh before they sold out. Four loko Neutra craft beer bitters heirloom aesthetic paleo, authentic street art cardigan flannel tilde selvage drinking vinegar. Food truck try-hard messenger bag, disrupt bespoke Marfa blog swag butcher ethical. Freegan four dollar toast before they sold out, Godard sartorial Banksy scenester cuddle Odd Future meh pour-over pickled taxidermy photo booth fulfill Marfa heirloom. Salvia you probably haven't heard of them Tumblr cliche, tofu Shoreditch distillery authentic swag. Sriracha fanny pack bitters try-hard, seitan taxidermy gluten-free tousled heirloom. Direct trade VHS cronut meggings, banh mi occupy fap Shoreditch messenger bag sustainable church-key expenditure blog readymade."
            
            fastFinder.findTagsInString(subject)
            
        }
    }
    
}
