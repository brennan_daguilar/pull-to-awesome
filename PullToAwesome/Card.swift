//
//  Card.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/1/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

struct Card {
    
    let id:Int
    let author:User
    let text:String


}
