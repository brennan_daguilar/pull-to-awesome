//
//  LinearPullEffect.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/2/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

class LinearPullEffect: TransformEffect {
   
    override func applyTransform(amount: CGFloat) {
        if let view = self.view {
            if let constraint = view.topPositionConstraint {
                // Cap the pull amount at 1 to prevent overdoing the position compensation
                let cappedAmount = min(1, amount)
                let fullDistance = controller != nil ? controller!.pullDistanceToCreate : view.frame.height
                let constant = -view.frame.height + (fullDistance * cappedAmount) - UIApplication.sharedApplication().statusBarFrame.size.height
                constraint.constant = constant
//                view.alpha = amount
            }
        }
    }
    
    override func removeTransform() {
        if let view = self.view {
//            view.alpha = 1
            view.topPositionConstraint?.constant = -view.bounds.height
        }
    }
    
}
