//
//  RequestTag.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 1/30/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

struct RequestTag {
    let id:Int
    let word:String
    let key:String
    let category:String
    let used:Int
    let active:Int
}
