//
//  BaseCardView.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/2/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

class BaseCardView: UIView {

    var strokeLayer:CAShapeLayer!
    var mask:CAShapeLayer!
    
    
    // Adds rounded corners and an outline
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(roundedRect: bounds, cornerRadius: 15).CGPath
        if mask == nil {
            mask = CAShapeLayer()
            mask.fillColor = UIColor.blackColor().CGColor
            layer.mask = mask
        }
        
        if strokeLayer == nil {
            strokeLayer = CAShapeLayer()
            layer.addSublayer(strokeLayer)
            strokeLayer.fillColor = UIColor.clearColor().CGColor
            strokeLayer.strokeColor = UIColor(white: 0.9, alpha: 1).CGColor
            strokeLayer.lineWidth = 2;
        }
        mask.path = path
        strokeLayer.path = path
        setNeedsDisplay()
        
        
    }

}
