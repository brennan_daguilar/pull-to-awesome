//
//  ComplexTagFinder.swift
//  PullToAwesome
//
//  This version of the TagFinder sorts each known tag into a dictionaries
//  based on the first letter.  This lets us limit the number of tags we
//  search for each word and drastically speeds up identification of tags
//  when a large number of tags are loaded into memory
//
//  Created by Brennan D'Aguilar on 1/25/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

class ComplexTagFinder: TagFinder {
    
    var sortedTags: Dictionary<Character, Dictionary<String, RequestTag>> = [:]

    override func clearTags() {
        sortedTags = [:]
    }
    
    override func addTags(tags:[RequestTag]) {
        var foundTags:[String] = []
        
        for tag in tags {
            let tagWord = isCaseSensitive ? tag.word : tag.word.lowercaseString
            let firstLetter = tagWord[tagWord.startIndex]
            
            var bucket: Dictionary<String, RequestTag>
            if let existingBucket = sortedTags[firstLetter] {
                bucket = existingBucket
            } else {
                sortedTags[firstLetter] = Dictionary<String, RequestTag>()
                bucket = sortedTags[firstLetter]!
            }
            
            if !contains(bucket.keys, tagWord) {
                sortedTags[firstLetter]![tagWord] = tag
                foundTags.append(tagWord)
            }
        }
    }
    
    override func isWordTag(word:String) -> Bool {
        if let tagBucket = self.sortedTags[word[word.startIndex]] {
            return contains(tagBucket.keys, word)
        } else {
            return false
        }
    }

    
}
