//
//  CardView.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/1/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

@IBDesignable
class CardView: BaseCardView {

    @IBOutlet var photo: UIImageView!
    @IBOutlet var textView:UITextView!
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var backgroundPhoto: UIImageView!

    
    
}
