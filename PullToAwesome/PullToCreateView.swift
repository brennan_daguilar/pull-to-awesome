//
//  PullToCreateView.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 1/25/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit


class PullToCreateView: BaseCardView, UITextViewDelegate {
    
    let pullingMessage = "Pull to Create"
    let waitingToReleaseMessage = "Release to Create"
    
    @IBOutlet var textView: UITextView!
    @IBOutlet var directionsLabel: UILabel!
    @IBOutlet var topPositionConstraint:NSLayoutConstraint!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var createButton: UIButton!
    @IBOutlet var charCountLabel: UILabel!
    
    
    var tagFinder: TagFinder?
    var baseTextAttributes:[NSObject:AnyObject]?
    var controller: PullToActionController?
    
    var state: PullToActionController.State {
        didSet {
            updateViewForCurrentState()
        }
    }
    
    override init() {
        state = .Pulling
        super.init()
        tagFinder = TagFinder(caseSensitive: false)
        baseTextAttributes = [NSFontAttributeName: UIFont.systemFontOfSize(17)]
    }
    
    override init(coder aDecoder: NSCoder) {
        state = .Pulling
        super.init(coder: aDecoder)
        tagFinder = TagFinder(caseSensitive: false)
        baseTextAttributes = [NSFontAttributeName: UIFont.systemFontOfSize(17)]
    }
    
    
    // Based on the current state, hide and show the appropriate controls
    func updateViewForCurrentState() {
        
        directionsLabel.hidden = !(state == .Pulling || state == .FullyPulled)
        directionsLabel.text = state == .Pulling ? pullingMessage : waitingToReleaseMessage
        textView.hidden = state != .Creating
        cancelButton.hidden = state != .Creating
        createButton.hidden = state != .Creating
        charCountLabel.hidden = state != .Creating

        if state == .Creating {
            textView.becomeFirstResponder()
        }
        if (state == .Closing) {
            textView.text = ""
            updateCharacterCount()
        }
        
    }
    
    // Cancel creation and dismiss the create view
    @IBAction func cancel() {
        if textView.isFirstResponder() {
            textView.resignFirstResponder()
        }
        controller?.cancelCreating()
    }
    
    // Block create actions if the textbox is empty.  When properly implemented,
    // this should also wait until the server has accepted the request before closing
    // the view
    @IBAction func create() {
        if countElements(textView.text) == 0 {
            return
        }
        
        if textView.isFirstResponder() {
            textView.resignFirstResponder()
        }
        controller?.finishCreating()
    }
    
    
    // MARK: - TextView
    func updateCharacterCount() {
        let numChars = countElements(textView.text)
        charCountLabel.text = "\(numChars)/200"
        charCountLabel.textColor = numChars > 200 ? UIColor.redColor() : UIColor.blackColor()
    }
    
    func textViewDidChange(textView:UITextView) {
        
        updateCharacterCount()
        
        if let finder = tagFinder {
            
            // Send the input string to the API and get back tags
            API.searchForTagsInRequestString(textView.text, completion: { (tags, error) -> Void in
                
                var attributedText = NSMutableAttributedString(string: textView.text, attributes: self.baseTextAttributes)
                
                // If the server found tags and returned successfully
                if let tags = tags {
                    finder.clearTags()
                    finder.addTags(tags)
                    
                    // Find the ranges of each word that matches a found tag
                    if let tagRanges = finder.findTagsInString(textView.text) {
                        
                        // Apply underline effect to all detected tags
                        for range in tagRanges {
                            attributedText.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
                        }
                    }
                }
                
                // Before replacing the attributed text, store the cursor position so we can restore it after
                let selectedRange = textView.selectedRange
                textView.attributedText = attributedText
                textView.selectedRange = selectedRange
            })
        }
    }
    
    // Watch for a single newline character.  This happens when the user presses the done button on the keyboard
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            create()
            return false
        }
        return true
    }

}
