//
//  PullToActionController.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/2/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit


class PullToActionController: NSObject {
    
    enum State {
        case Pulling, FullyPulled, Opening, Creating, Closing
    }
    
    var startAction:(()->())?
    var endAction:(()->())?
    var pullDistanceToCreate:CGFloat = 100
    var createView:PullToCreateView
    var parentView:UIView
    
    var effect:TransformEffect? {
        willSet(newEffect) {
            if let currentEffect = effect {
                currentEffect.applyTransform(0)
                currentEffect.view = nil
                currentEffect.controller = nil
            }
        }
        didSet(previousEffect) {
            effect?.view = createView
            effect?.controller = self
            effect?.applyTransform(0)
        }
    }
    
    var state:State {
        didSet {
            createView.state = state
        }
    }
    
    init(createView:PullToCreateView, parentView:UIView, distance:CGFloat, startAction:()->(), endAction:()->()) {
        self.state = .Pulling
        self.createView = createView
        self.startAction = startAction
        self.endAction = endAction
        self.pullDistanceToCreate = distance
        self.parentView = parentView
        self.effect = LinearPullEffect()
        
        // Observers don't get called while initializing
        self.effect?.view = createView
        
        super.init()
        self.createView.controller = self
        self.effect?.controller = self
        self.effect?.applyTransform(0)
    }
    
    func updatePullAmount(pan:CGPoint, animated:Bool = false) {
        let amount = pan.y / pullDistanceToCreate
        
        var endState:State = self.state
        var shouldBeHidden = amount <= 0
        if !shouldBeHidden {
            self.createView.hidden = false
        }
        let changes:()->() = {
            if self.state == .Creating {
                return
            }
            
            if self.state != .Closing && amount >= 1 {
               endState = .FullyPulled
            } else {
                endState = .Pulling
            }
            self.effect?.applyTransform(amount)
            self.createView.layoutIfNeeded() // Maybe need parent view
            return
        }
        
        //        layoutIfNeeded()
        if animated {
            UIView.animateWithDuration(0.3, animations: changes, completion: { (finished) -> Void in
                self.state = endState
                self.createView.hidden = shouldBeHidden
            })
        } else {
            changes()
            self.state = endState
            self.createView.hidden = shouldBeHidden
        }
    }
    
    
    // If the user has pulled enough, show the create view.  Otherwise animate the view back off screen
    func didEndDragging() {
        if state == .FullyPulled {
            if let action = self.startAction {
                action()
            }
            self.state = .Opening
            createView.layoutIfNeeded()
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.createView.topPositionConstraint.constant = 0
                self.createView.layoutIfNeeded()
            }, completion: { (finished) -> Void in
                self.state = .Creating
            })

        } else {
            updatePullAmount(CGPointZero, animated: true)
        }
    }
    
    func cancelCreating() {
        close()
        
    }
    func finishCreating() {
        close()
    }
    
    func close() {
        if state == .Creating {
            state = .Closing
            updatePullAmount(CGPointZero, animated: true)
            if let action = self.endAction {
                action()
            }
        }
    }
    
    
}
