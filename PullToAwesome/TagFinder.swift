//
//  TagFinder.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 1/30/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

class TagFinder: NSObject {
    
    let charSet = NSCharacterSet(charactersInString: " ,.!:")
    let isCaseSensitive:Bool
    
    var tags: [String: RequestTag] = [:]

    init(caseSensitive:Bool) {
        isCaseSensitive = caseSensitive
    }
    
    // MARK: - Configuring Tags
    func addTags(newTags: [RequestTag]) {
        for tag in newTags {
            let tagWord = isCaseSensitive ? tag.word : tag.word.lowercaseString
            if !contains(tags.keys, tagWord) {
                tags[tagWord] = tag
            }
        }
    }
    
    func clearTags() {
        tags = [:]
    }
    
    // MARK: - Finding Tags
    
    /*
    Splits the input string into words separated by known 
    delimeters and returns the range of each word in the 
    original string
    */
    func findTokensInString(string: NSString) -> [NSRange]? {
        var startIndex = 0
        var nextWhitespace:NSRange
        var potentialRanges = [NSRange]()
        
        do {
            let lengthRemaining = string.length - startIndex
            let searchRange = NSRange(location: startIndex, length: lengthRemaining)
            
            nextWhitespace = string.rangeOfCharacterFromSet(charSet, options: NSStringCompareOptions.CaseInsensitiveSearch, range: searchRange)
            
            var wordEndIndex = string.length
            if nextWhitespace.location != NSNotFound {
                wordEndIndex = nextWhitespace.location
            }
            let range = NSRange(location: startIndex, length: wordEndIndex - startIndex)
            potentialRanges.append(range)
            
            startIndex = wordEndIndex + 1
        } while startIndex < string.length
        return potentialRanges.isEmpty ? nil : potentialRanges
    }
    
    // Helpful method to convert Swift's String class to an NSString
    func findTagsInString(string: String) -> [NSRange]? {
        return findTagsInString(string as NSString)
    }
    
    /*
    Searches each word in the input string for known tags and returns the
    the range of each tag it finds
    */
    func findTagsInString(inputString: NSString) -> [NSRange]? {
        var start = NSDate()
        let string = isCaseSensitive ? inputString : inputString.lowercaseString as NSString
        var tagCount = 0
        var tagRanges = [NSRange]()
        if let potentialRanges = findTokensInString(string) {
            for range in potentialRanges {
                let word = string.substringWithRange(range)
                if !word.isEmpty {
                    if isWordTag(word) {
                        tagRanges.append(range)
                        tagCount++
                    }
                }
            }
        }
        
        var end = NSDate()
        var duration = String(format: "%.02fms", end.timeIntervalSinceDate(start) * 1000)
        println("Found \(tagCount) tags in \(duration)")
        
        return tagRanges.isEmpty ? nil : tagRanges
        
    }
    
    /* 
    Checks a given word against known tags.  Broken out to allow subclasses to
    detect tags differently without duplicating excess code
    */
    func isWordTag(word:String) -> Bool {
        return contains(tags.keys, word)
    }

   
}
