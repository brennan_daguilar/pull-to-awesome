//
//  CardGenerator.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/1/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

class CardGenerator: NSObject {
    
    class func getCards() -> [Card] {
        
        let names = ["Ethan", "Mike", "Mary", "Jacob"]
        let requests = [
            "Looking to chat with visual UI/UX designers about how to improve the aesthetic feel of the iOS app I'm building",
            "Looking to collaborate with an animator on a short animated promo video I'm making for a card game",
            "Looking to connect with SF/Silicon Valley-based Product Managers to get advice about making the transition from Canada to SV.",
            "Looking for feedback on a travel app I'm building - http://cotripping.com. Do you like the idea, would you use it, what features would you like? Thanks in advance."
        ]
        
        var cards = [Card]()
        for i in 0...3 {
            let user = User(id: i, name: names[i], photo: names[i], burredPhoto: names[i] + "Blurred")

            cards.append(Card(id: i, author:user, text: requests[i]))
        }
        
        return cards
    }
   
}
