//
//  API.swift
//  PullToAwesome
//
//  Using two popular libraries: Alamofire and SwiftyJSON to handle http requests
//  and JSON parsing respectively.
//
//  Created by Brennan D'Aguilar on 1/29/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

class API: NSObject {
    
    class func searchForTagsInRequestString(string:String, completion:([RequestTag]?, NSError?) -> Void) {
//        completion(nil,nil)
//        return
        var test = request(.GET, "https://api.volley.works/search/string", parameters: ["string_query" : string]).responseJSON { (_, response, rawJson, error) -> Void in
            
            if (error != nil) {
                completion(nil, error)
                return
            } else {
                let json = JSON(rawJson!)
                var tags = [RequestTag]()
                for (index:String, tagJson: JSON) in json {
                    let id = tagJson["tag_id"].int
                    let word = tagJson["tag_word"].string
                    let key = tagJson["tag_key"].string
                    let category = tagJson["tag_category"].string
                    let used = tagJson["tag_used"].int
                    let active = tagJson["tag_active"].int
                    
                    switch (id, word, key, category, used, active) {
                    case let (.Some(id), .Some(word), .Some(key), .Some(category), .Some(used), .Some(active)):
                        let tag = RequestTag(id:id, word:word, key:key, category:category, used:used, active:active)
                        tags.append(tag)
                    default:
                        completion(nil, NSError(domain: "JSON Parsing", code: 1, userInfo: ["json": json.rawValue]))
                        return
                    }
                }
                //                println(tags.count)
                let parsedTags:[RequestTag]? = tags.count > 0 ? tags : nil
                completion(parsedTags, nil)
            }
        }
    }
    
    class func getCards(completion:([Card]?, NSError?) -> Void) {

        delay(0.5, {
            completion(CardGenerator.getCards(), nil)
        })
    }
}
