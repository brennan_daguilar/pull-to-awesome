//
//  CardViewController.swift
//  PullToAwesome
//
//  Created by Brennan D'Aguilar on 2/1/15.
//  Copyright (c) 2015 Brennan D'Aguilar. All rights reserved.
//

import UIKit

class CardViewController: UIViewController, UIGestureRecognizerDelegate {
    
    enum DragDirection {
        case Horizontal, Vertical
    }
    
    let dragThreshold:CGFloat = 100;
    
    @IBOutlet var panGestureRecognizer: UIPanGestureRecognizer! // Used to detect both swiping and pull to create gestures
    @IBOutlet var pullToCreateView: PullToCreateView!
    
    // Contains the pull to create view.  This view's size is adjusted to fit the available space above the keyboard
    // The pull to create view is automatically adjusted to fit this container
    @IBOutlet var createContainer: UIView!
    
    // Used to change the bottom point of the create container whent the keyboard is shown or hidden
    @IBOutlet var createContainerBottomConstraint: NSLayoutConstraint!
    
    // Layered on top of the swipable cards and below the createContainer.  Used to blur out the background when creating a request
    @IBOutlet var blurView: UIVisualEffectView!
    
    // Once a pan gesture begins it is locked to either horizontal (swiping cards) or vertical (pull to create)
    var dragDirection = DragDirection.Horizontal
    
    var cards = [Card]()
    var cardViews = [CardView]()
    var currentCardIndex = 0
    let animationSpeed = 0.2
    let offscreenDistance:CGFloat = 1.5
    var pullToCreateController:PullToActionController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.blurView.hidden = true
        
        // Set up the controller that manages the pull to create gesture
        pullToCreateController = PullToActionController(createView: pullToCreateView, parentView: self.createContainer, distance: 100, startAction: willShowCreateView, endAction: willHideCreateView)
        
        // Detect keyboard changes
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillChange:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillChange:", name: UIKeyboardDidChangeFrameNotification , object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification , object: nil)
        
        
        // Initialize CardViews
        setupCardViews()

        
        self.view.layoutIfNeeded()
        self.view.bringSubviewToFront(self.createContainer)
    }
    
    override func viewWillAppear(animated: Bool) {
        // Start Loading card data from the server
        updateVisibleCardViews()
        if (cards.count == 0) { // Since data is mocked, refreshing doesn't really help anything
            API.getCards { (newCards, error) -> Void in
                if let cards = newCards {
                    self.updateCards(cards)
                }
            }
        }
    }
    
    
    // When receiving new cards from the server, animates the visible card out, then
    // sets up the new card data, resets to the first card in the new data, and animates it
    // back on screen
    func updateCards(cards:[Card]) {
        let previousCardView = getCurrentCardView()
        
        let animations = { () -> Void in
            previousCardView.alpha = 0
            return
        }
        
        UIView.animateWithDuration(animationSpeed, animations:animations) { (finished) -> Void in
            previousCardView.alpha = 1
            
            self.cards = cards
            self.currentCardIndex = 0
            self.updateVisibleCardViews()
            self.resetCardPosition(animated: false)
            let newCurrentCardView = self.getCurrentCardView()
            newCurrentCardView.alpha = 0
            
            UIView.animateWithDuration(self.animationSpeed, animations: { () -> Void in
                newCurrentCardView.alpha = 1
            })
            return
        }
        
        
    }

    // MARK: - Card Views
    
    
    /*
    Creates 3 card views that are reused for the lifetime of this view controller.  The views are cycled so that the next and previous cards
    are prepared and able to be shown at the same time.  The current setup of the swiping animations makes this currently unnecessary as only
    one card is ever on screen at a time.  But this allows for easy experimentation with different timings and transitions.
    */
    func setupCardViews() {
        for i in 0..<3 {
            // The card view is set up in a separate nib to avoid having extra unneeded view controllers
            let cardView = NSBundle.mainBundle().loadNibNamed("CardView", owner: self, options: nil)[0] as (CardView)
            cardView.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            cardViews.append(cardView)
            self.view.insertSubview(cardView, belowSubview: blurView)
            
            // Constrains the max size to 300x500
            let maxHeight = NSLayoutConstraint(item: cardView, attribute: .Height, relatedBy: .LessThanOrEqual, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 500)
            let maxWidth = NSLayoutConstraint(item: cardView, attribute: .Width, relatedBy: .LessThanOrEqual, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 300)
            
            // Sets the size to 90% of the container view's size.  The lower priority allows the above
            // constraints to limit the size if the container is too big
            let fillHeight = NSLayoutConstraint(item: cardView, attribute: .Height, relatedBy: .Equal, toItem: self.view, attribute: .Height, multiplier: 0.9, constant: 0)
            fillHeight.priority = 900
            let fillWidth = NSLayoutConstraint(item: cardView, attribute: .Width, relatedBy: .Equal, toItem: self.view, attribute: .Width, multiplier: 0.9, constant: 0)
            fillWidth.priority = 900
            let constraints = [maxHeight, maxWidth, fillHeight, fillWidth]
            
            self.view.addConstraints(constraints)
        }
    }
    
    // Gets the card view that should be used for a card at a given index
    func getCardView(#index: Int) -> CardView {
        return cardViews[index % 3]
    }
    
    // Gets the card view based on the currently set index (for convenience)
    func getCurrentCardView() -> CardView {
        return getCardView(index: currentCardIndex)
    }
    
    // Updates the contents of each card view and hides them if no card data has been loaded or
    // if they're not the current card.  This is somewhat repetitive as it sets the contents of the
    // current card which should already be set.  Since this is a simple view, being a little inefficient
    // isn't the end of the world.  If things get more complicated it might be worth revisiting.
    func updateVisibleCardViews() {
        let currentCardView = getCurrentCardView()
        
        updateCardDetails(index: currentCardIndex)
        updateCardDetails(index: currentCardIndex + 1)
        updateCardDetails(index: currentCardIndex - 1)
        
        let noLoadedCards = cards.count == 0
        for cardView in cardViews {
            cardView.hidden = noLoadedCards || cardView != currentCardView
        }
        
        self.view.layoutIfNeeded()
    }
    
    // Displays the card data in the appropriate card view
    func updateCardDetails(#index:Int) {
        
        // As a safety check, only allow the previous, current, and next card indexes
        // to be used to update the views
        let validIndex = index >= 0 && index <= cards.count - 1
        let withinRange = index >= currentCardIndex-1 && index <= currentCardIndex+1
        
        if validIndex && withinRange {
            let cardView = getCardView(index: index)
            let card = cards[index]
            cardView.textView.selectable = true
            cardView.textView.text = card.text
            cardView.textView.selectable = false
            cardView.nameLabel.text = card.author.name
            cardView.backgroundPhoto.image = UIImage(named: card.author.burredPhoto)
            cardView.photo.image = UIImage(named: card.author.photo)
        }
    }

    
    
    // MARK: - Keyboard Adjustments
    /*
    Called when the keyboard is shown or changes in size.  This calculates
    how much the keyboard will overlap the bottom of the view (on ipads this
    isn't just the bottom of the screen as the view is a modal) and adjusts a
    constraint to keep the characters remaining label from being overlapped.
    This also pushes up the bottom of the text view because constraints are awesome.
    */
    func keyboardWillChange(notification:NSNotification) {
        let userInfo = notification.userInfo!
        
        if let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
            let bottomLeft = CGPoint(x: view.frame.minX, y: view.frame.maxY)
            let bottomEdgePosition = self.view.convertPoint(bottomLeft, toView: self.view.window)
            let overlap = max(0, bottomEdgePosition.y - frame.minY)
            
            self.view.layoutIfNeeded()
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.createContainerBottomConstraint.constant = max(0, overlap)
                self.view.layoutIfNeeded()
            })
        }
    }
    
    // Called when the keyboard is hiding and returns the views to their starting point
    func keyboardWillHide(notification:NSNotification) {
        let userInfo = notification.userInfo!
        
        if let duration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue {
            view.layoutIfNeeded()
            self.createContainerBottomConstraint.constant = 0
            UIView.animateWithDuration(duration, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    // MARK: - Panning
    @IBAction func didPan(recognizer: UIPanGestureRecognizer) {
        
        let pan = recognizer.translationInView(self.view)
        let currentCardView = getCurrentCardView()
        
        switch recognizer.state {
        case .Began:
            // based on the initial velocity, lock this interaction to horizontal or vertical
            // Gives priority to horizontal since horizontal swiping is the more common gesture.
            // Vertical swipes have to be fairly deliberate to trigger
            let velocity = recognizer.velocityInView(self.view)
            dragDirection = abs(velocity.x) > (abs(velocity.y) / 3) ? .Horizontal : .Vertical
        case .Changed:
            if dragDirection == .Horizontal {
                moveCard(index: currentCardIndex, pan: pan)
            } else {
                pullToCreateController?.updatePullAmount(pan)
            }
        case .Ended:
            if (dragDirection == DragDirection.Horizontal) { // Swiping
                
                // Check if the card has been dragged enough to trigger the next/prev transition
                let pannedEnough = abs(pan.x) > dragThreshold
                if  pannedEnough {
                    let frameWidth = self.view.frame.width
                    let forward = pan.x < 0
                    
                    // Check if there's a next card to go to in the swipped direction
                    var hasNextCard:Bool
                    if forward {
                        hasNextCard = currentCardIndex < cards.count - 1
                    } else {
                        hasNextCard = currentCardIndex > 0
                    }
                    
                    if !hasNextCard {
                        resetCardPosition(animated: true) // If there's no next card, return the current card to the center
                    } else {
                        let endX:CGFloat = (forward ? -frameWidth : frameWidth) * offscreenDistance
                        
                        // Block interactions while the cards are animating
                        panGestureRecognizer.enabled = false
                        UIView.animateWithDuration(animationSpeed, animations: { () -> Void in
                            self.moveCard(index: self.currentCardIndex, pan: CGPoint(x: endX, y: pan.y))
                            }, completion: { (finished) -> Void in
                                self.panGestureRecognizer.enabled = true
                                if (forward) {
                                    self.nextCard()
                                } else {
                                    self.previousCard()
                                }
                        })
                    }
                } else { // Didn't swipe far enough
                    resetCardPosition(animated: true)
                }
            } else { // Pull to create
                pullToCreateController?.didEndDragging()
            }
        default: break
        }
        
    }
    
    // Gets the center point to return card views to.  self.view.center gives its center point in
    // reference to its parents view, not the center point in its own bounds
    func getCenterPoint() -> CGPoint {
        let center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
        return center
        
    }
    
    // Moves the card to the appriopriate point on the screen and rotates it based on
    // how far from center it is
    func moveCard(#index: Int, pan: CGPoint) {
        let cardView = getCardView(index: index)
        let centerPoint = getCenterPoint()
        let newCenter = CGPoint(x: centerPoint.x + pan.x, y: centerPoint.y + (pan.y / 4))

        cardView.center = newCenter
        let pull = max(-1, min(1, pan.x / self.view.frame.width))
        let maxRotation:CGFloat = CGFloat(M_PI/4)
        let rotationAmount = maxRotation * pull
        cardView.transform = CGAffineTransformMakeRotation(rotationAmount)
    }
    
    // For convienence, this moves the current card back to the center point
    func resetCardPosition(#animated:Bool) {
        let currentCardView = getCurrentCardView()
        let changes = { () -> Void in
            self.moveCard(index: self.currentCardIndex, pan: CGPointZero)
        }
        if animated {
            UIView.animateWithDuration(animationSpeed, animations: changes)
        } else {
            changes()
        }
    }
    
    // Animates the next card onto the screen from the right
    func nextCard() {
        if currentCardIndex >= cards.count - 1 {
            return
        }
        
        currentCardIndex++
        updateVisibleCardViews()
        
        var startingCenter = getCenterPoint()
        startingCenter.x += self.view.frame.width * offscreenDistance
        moveCard(index: currentCardIndex, pan: startingCenter)
        
        UIView.animateWithDuration(animationSpeed, animations: { () -> Void in
            self.moveCard(index: self.currentCardIndex, pan: CGPointZero)
        })
    }
    
    // Animates the previous card onto the screen from the left
    func previousCard() {
        if currentCardIndex == 0 {
            return
        }
        
        currentCardIndex--
        updateVisibleCardViews()
        
        var startingCenter = getCenterPoint()
        startingCenter.x -= self.view.frame.width * offscreenDistance
        moveCard(index: currentCardIndex, pan: startingCenter)
        
        UIView.animateWithDuration(animationSpeed, animations: { () -> Void in
            self.moveCard(index: self.currentCardIndex, pan: CGPointZero)
        })
    }
    
    // When rotating the screen, adjust the visible card to the correct position.
    // The visible card would be in the incorrect position normally
    // due to manually adjusting position instead of using constraints.
    // Migrating the cards to being fully constraint based would make this unnecessary
    override func viewDidLayoutSubviews() {
        resetCardPosition(animated: false)
    }
    
    
    // MARK: - Creating
    
    // When the create view is showing, block swipes and blur the background
    func willShowCreateView() {
        self.panGestureRecognizer.enabled = false
        self.blurView.hidden = false
    }
    
    // When the create view is hiding, reenable swipes and unblur the background
    func willHideCreateView() {
        self.panGestureRecognizer.enabled = true
        self.blurView.hidden = true
    }
    
    
}
